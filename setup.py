from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='poplar_config',
    version='0.0.2',
    description='Flexible configuration for Extender.',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://polars.dev',
    author='Poplar Development',
    author_email='chris@poplars.dev',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3.4',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    ],
    keywords='sage orchid extender',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=[],
    extras_require={
    },
    package_data={
        'poplar_config': ['expi.json', 'vi/*.vi',],
    },
)

