import json
import datetime

from extools.view import ExView
from extools.view.exsql import exsql_result
from extools.view.errors import ExViewError
from extools.message import ExMessages

exm = ExMessages("poplar_config", level=ExMessages.INFO)


def json_default(obj):
    if isinstance(obj, datetime.datetime):
        return {'__dt__': obj.isoformat()}
    return obj


def json_objhook(obj):
    if '__dt__' in obj:
        return datetime.datetime.strptime(
                obj['__dt__'], '%Y-%m-%dT%H:%M:%S.%f')
    return obj

class PoplarConfig(object):
    VIEW_NAME = "PPCONFIG.PPCONFIG"

    def __init__(self, module, company):
        self.company = company
        self.module = module
        self._view = None
        exm.debug("Starting Config for {} in {} [{}]".format(
            self.module, self.company, self._next_id))

    @property
    def _next_id(self):
        _id = None
        max = 0
        try:
            with exsql_result(
                    "SELECT MAX(ID)+1 AS MAXID FROM PPCONFIG") as result:
                return result.get("MAXID")
        except ExViewError as e:
            exm.debug("Error getting id: {}".format(e))
            return 0

    @property
    def view(self):
        if not self._view:
            exm.debug("Getting view {}".format(self.VIEW_NAME))
            self._view = ExView(self.VIEW_NAME)
        return self._view

    def exists(self, key, user=""):
        try:
            self.view.seek_to(
                    COMPANY=self.company, MODULEID=self.module,
                    USERID=user, KEY=key)
            value = self.view.get("VALUE")
        except ExViewError as err:
            return False

        return True

    def get(self, key, user="", default=None):
        try:
            exm.debug("Seeking to {}, {}, {}, {}".format(
                self.company, self.module, user, key))
            self.view.seek_to(
                    COMPANY=self.company, MODULEID=self.module,
                    USERID=user, KEY=key)
            return json.loads(self.view.get("VALUE"),
                              object_hook=json_objhook)
        except ExViewError as e:
            exm.debug("View Error: {}/{}".format(e, repr(e)))
            return default
        except ValueError as e:
            return default

    def get_for_user_or_default(self, key, user, default=None):
        g = self.get(key, user)
        if g:
            return g

        g = self.get(key)
        if g:
            return g

        return default

    def get_or_set(self, key, value=None, user=""):
        g = self.get(key, user, None)
        if g:
            return g

        return self.set(key, value, user)

    def set(self, key, value, user=""):
        exists = self.exists(key, user)
        exm.debug("Setting {} to {} for {}. Exists: {}".format(
            key, value, user, exists))

        if not self.exists(key, user):
            self.view.recordGenerate()
            self.view.put("ID", self._next_id)
            self.view.put("COMPANY", self.company)
            self.view.put("MODULEID", self.module)
            self.view.put("KEY", key)
            self.view.put("USERID", user)
            self.view.put("VALUE",
                          json.dumps(
                                value, default=json_default))
            self.view.insert()
        else:
            self.view.put("VALUE",
                          json.dumps(
                                value, default=json_default))
            self.view.update()

        return value

    def delete(self, key, user=None):
        try:
            if self.exists(key, user):
                self.view.delete()
                return True
        except ExViewError as e:
            pass
        return False

    def __str__(self):
        return "Config for Module {} Company {}".format(
            self.module, self.company)
