from extools.errors import ExToolsError
from extools.test import ExTestCase

from poplar_config import PoplarConfig

try:
    from accpac import *
except ImportError:
    pass

class PoplarConfigTestCase(ExTestCase):
    """Test the functions in :py:mod:`poplar_config` module."""

    def test_new_set(self):
        """Verify that set updates valid key/value to the database."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")

    def test_update_set(self):
        """Verify that set update valid key/value to the database."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_equal(pc.set("test", "test1"), "test1")

    def test_get(self):
        """Verify that values are retrieved correctly."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_equal(pc.get("test"), "test")
        self.assert_equal(pc.get("fake"), None)
        self.assert_equal(pc.get("fake", None, "test"), "test")

    def test_delete(self):
        """Verify that values are deleted."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_true(pc.delete("test"))
        self.assert_true(not pc.delete("test"))

    def test_delete_user_specific_key(self):
        """Verify that values are deleted."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_equal(pc.set("test", "FAKE", "FAKE"), "FAKE")
        self.assert_true(pc.delete("test", "FAKE"))
        self.assert_equal(pc.get("test"), "test")

    def test_module_required(self):
        """Verify that the module name is a required field."""
        with self.assert_raises(TypeError):
            pc = PoplarConfig(company=org)

    def test_company_required(self):
        """Verify that the company name is a required field."""
        with self.assert_raises(TypeError):
            pc = PoplarConfig("PPCONFIG-TEST")

    def test_user_specific_value(self):
        """Verify that user specific options are returned when requested."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_equal(pc.set("test", "my test", user=user), "my test")
        self.assert_equal(pc.set("test", "my test", user="FAKE"), "my test")
        self.assert_equal(pc.get("test"), "test")
        self.assert_equal(pc.get("test", user=user), "my test")
        self.assert_equal(pc.get("test", user="FAKE"), "my test")

    def test_get_for_user_or_default(self):
        """Verify that user specific options are returned when requested."""
        pc = PoplarConfig("PPCONFIG-TEST", org)
        self.assert_equal(pc.set("test", "test"), "test")
        self.assert_equal(pc.get("test"), "test")

        self.assert_equal(pc.get_for_user_or_default("test", "FAKE"), "test")
        self.assert_equal(pc.get_for_user_or_default("FAKE", "FAKE", True), True)
        self.assert_equal(pc.get_for_user_or_default("FAKE", "FAKE"), None)


def main(*args, **kwargs):
    """This main hook is picked up by ExTestRunner for automatic execution."""
    ext = PoplarConfigTestCase()
    ext.run()
